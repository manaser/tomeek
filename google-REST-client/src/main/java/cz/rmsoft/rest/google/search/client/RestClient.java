package cz.rmsoft.rest.google.search.client;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

public class RestClient {
    Client client = ClientBuilder.newClient();
    WebTarget target = client.target("https://www.googleapis.com").path("customsearch/v1");

    static final String googleApiKey="AIzaSyA6IouZ6_LnvPIUnA3dj6AKhEoPmZDB9tw";
    static final String customSearch="017576662512468239146:omuauf_lfve";

    public String queryGoogleForSearchExpression(String expression) {
        Response response = target.queryParam("key", googleApiKey)
                .queryParam("q", expression)
                .queryParam("cx", customSearch)
                .request().get();
        return response.readEntity(String.class);
    }

}
