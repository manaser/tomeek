package cz.rmsoft.rest.google.search.client;


/**
 * Hello world!
 *
 */
public class App
{
    public static void main( String[] args )
    {
        RestClient restClient = new RestClient();
        String searchExpression = String.join(" ", args);
        System.out.println(searchExpression);
        String hello = restClient.queryGoogleForSearchExpression(searchExpression);
        System.out.println(hello);
    }
}
